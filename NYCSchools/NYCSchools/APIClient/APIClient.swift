//
//  APIClient.swift
//  NYCSchools
//
//  Created by Siva Rama on 17/03/23.
//

import RxSwift

class APIClient {
    static func send<T: Codable>(request apiRequest: APIRequest) -> Single<T> {
        return Single<T>.create { observer in
            let request = apiRequest.request()
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    observer(.failure(error))
                    return
                }
                guard let data = data else {
                    observer(.failure(NetworkError.noData))
                    return
                }
                do {
                    let model: T = try JSONDecoder().decode(T.self, from: data)
                    observer(.success(model))
                } catch {
                    observer(.failure(NetworkError.jsonParse))
                }
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}
