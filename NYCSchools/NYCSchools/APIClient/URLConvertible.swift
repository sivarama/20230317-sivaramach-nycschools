//
//  URLConvertible.swift
//  NYCSchools
//
//  Created by Siva Rama on 17/03/23.
//

import Foundation

public protocol URLConvertible {
    func asURL() throws -> URL
}

extension String: URLConvertible {
    public func asURL() throws -> URL {
        guard let url = URL(string: self) else {
            throw NetworkError.invalidURL(self)
        }
        return url
    }
}
