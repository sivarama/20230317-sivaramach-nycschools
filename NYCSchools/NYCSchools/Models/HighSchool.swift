//
//  HighSchool.swift
//  NYCSchools
//
//  Created by Siva Rama on 17/03/23.
//

import Foundation

struct HighSchool: Codable {
    let dbn: String
    let schoolName: String
    let boro: String
    let overviewParagraph: String

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case boro = "boro"
        case overviewParagraph = "overview_paragraph"
    }
}
