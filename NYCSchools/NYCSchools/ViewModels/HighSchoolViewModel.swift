//
//  HighSchoolViewModel.swift
//  TradeInfy
//
//  Created by Siva Rama on 17/03/23.
//

import Foundation
import RxSwift
import RxCocoa

class HighSchoolViewModel {
    private(set) var schools = BehaviorRelay<[HighSchool]>(value: [])
    private(set) var error = BehaviorRelay<String>(value: "")
    private(set) var isLoading = BehaviorRelay<Bool>(value: false)
    private let disposeBag = DisposeBag()

    func fetchHighSchools() {
        print("fetching high schools")
        let request = HighSchoolsRequest(path: "s3k6-pzi2.json")
        isLoading.accept(true)
        let single: Single<[HighSchool]> = APIClient.send(request: request)
        single
            .observe(on: MainScheduler.instance)
            .subscribe { schools in
                self.schools.accept(schools)
                self.isLoading.accept(false)
            } onFailure: { errorValue in
                self.isLoading.accept(false)
                self.error.accept(errorValue.localizedDescription)
            }
            .disposed(by: disposeBag)
    }
    
    func noStatFound(_ newError: String) {
        error.accept(newError)
    }
}

struct HighSchoolsRequest: APIRequest {
    var method: HTTPMethod = .GET
    var path: String
    var parameters: [String : String]? = nil
    var body: [String : Any]? = nil
}
